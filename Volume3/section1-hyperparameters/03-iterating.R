library(mlbench)
library(caret)

data(Sonar)

X <- Sonar[, 1:(ncol(Sonar)-1)]
y <- as.factor(Sonar$Class)

train_idx <- createDataPartition(y, p = .7, 
                                 list = FALSE)
X_train <- X[train_idx, ]
y_train <- y[train_idx]

X_test <- X[-train_idx,]
y_test <- y[-train_idx]

tune_grid <- expand.grid(alpha = 60:90 / 10000, 
                         lambda = (85:120)/1000)
trctl <- trainControl(method = 'cv', 
                      number = 5, 
                      savePredictions = TRUE)
model <- train(x = X_train,
               y = y_train,
               tuneGrid = tune_grid,
               method = "glmnet",
               trControl = trctl)

print(model$bestTune)
