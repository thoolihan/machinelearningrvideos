library(caret)

idx <- createDataPartition(mtcars$vs, p = .7, list = FALSE)

train <- mtcars[idx,]
train$vs <- factor(train$vs, labels = c('V', 'S'))
test <- mtcars[-idx,]
test$vs <- factor(test$vs, labels = c('V', 'S'))

model <- caret::train(vs ~ mpg + cyl + hp + wt,
               data = train,
               method = "LogitBoost",
               preProcess = c('center', 'scale'))

y_pred <- predict(model, newdata = test)

caret::confusionMatrix(y_pred, test$vs)

saveRDS(model, file = "model.RDS")

model2 <- readRDS("model.RDS")

summary(model)

summary(model2)

tstring <- format(Sys.time(), format = '%Y.%m.%d.%H.%M.%S')

filename <- paste('model-', tstring, '.rds', sep = '')
filename
