import tensorflow as tf

a = tf.constant([1.0], shape = [1, 1], name = 'a')

sess = tf.Session(config = tf.ConfigProto(log_device_placement = True))

print(sess.run(a))
