#!/usr/bin/env bash

. ~/.virtualenvs/r-tensorflow/bin/activate
pip uninstall tensorflow
pip install tensorflow-gpu
