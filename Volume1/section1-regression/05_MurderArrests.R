set.seed(1)

m <- nrow(X)
train_index <- sample.int(m, size = m * .70)

train <- USArrests[train_index, ]
test <- USArrests[-train_index, ]

X_train <- train[,c('Assault', 'UrbanPop', 'Rape')]
X_test <- test[,c('Assault', 'UrbanPop', 'Rape')]
y_train <- train$Murder
y_test <- test$Murder

model <- lm(Murder ~ ., data = USArrests[train_index,])
summary(model)

# New Code

## predict test set
y_pred <- predict(model, test)

## add predictions and errors to test data.frame
test$Murder_predicted <- y_pred
test$Error <- test$Murder_predicted - test$Murder
test[, c('Murder', 'Murder_predicted', 'Error')]

## r-squared
r_squared <- function(pred, actual) {
  err <- pred - actual
  err_sq <- err ^ 2
  regress <- pred - mean(pred)
  regress_sq <- regress ^ 2
  return(1 - sum(err_sq) / sum(regress_sq))
}

r_squared(y_pred, test$Murder)

## RMSE Root Mean Squared Error
rmse <- function(pred, actual) {
  err <- pred - actual
  sq_err <- err ^ 2
  mean_sq_err <- mean(sq_err)
  return(sqrt(mean_sq_err))
}

rmse(y_pred, test$Murder)

## residual plot
library(ggplot2)

model_residuals <- resid(model)
test_residuals <- y_pred - test$Murder
qplot(x = train$Assault, y = model_residuals, 
      xlab = "Assault", ylab = "Residual", color = 'Train Data') +
  geom_point(aes(x = test$Assault, 
                 y = test_residuals, color='Test Data')) + 
  geom_abline(intercept = c(0, 0), slope = 0) +
  scale_y_continuous(limits = c(-6, 6))
