.PHONY: clean Vol*.zip

Vol1.zip:
	zip -r $@ Volume1

Vol2.zip:
	zip -r $@ Volume2

Vol3.zip:
	zip -r $@ Volume3

clean:
	rm -f Vol1.zip Vol2.zip Vol3.zip
